# README #

## Pivotal Web Services
## Sign up
Für die Registrierung ist eine Email-Adresse + Handynummer erforderlich.

https://account.run.pivotal.io/z/uaa/sign-up

## CLI
https://docs.run.pivotal.io/cf-cli/install-go-cli.html

## Login
- (CMD) cf login -a https://api.run.pivotal.io -u <vorname.nachname>@pentasys.de
- (WEB) https://console.run.pivotal.io

## Run / Build
- ./gradlew(.bat) clean build
- ./gradlew(.bat) bootRun

## Push
- Der Applikationsname muss eindeutig sein, um eine öffentliche Route zu bekommen. Am Besten in spring-boot-pentasys-<Benutzerkürzel> in der manifest.yml ändern.
- cf push

## API
- (Local) localhost:8080/api/denial-of-service?loop-count=100
- (PCF) https://spring-boot-pentasys-<Benutzerkürzel>/api/denial-of-service?loop-count=100

## Befehle
### App Autoscaler
- https://docs.cloudfoundry.org/devguide/services/managing-services.html
- https://docs.pivotal.io/pivotalcf/2-0/appsman-services/autoscaler/using-autoscaler.html#metric
- https://docs.run.pivotal.io/appsman-services/autoscaler/using-autoscaler-cli.html

## App Autoscaler Install / Service
- cf marketplace
- cf create-service app-autoscaler standard autoscaled-app (cf create-service SERVICE PLAN SERVICE_INSTANCE)
- cf services
- cf service autoscaled-app
- cf bind-service spring-boot-pentasys autoscaled-app (cf bind-service APP SERVICE_INSTANCE)

## Logout
- cf logout