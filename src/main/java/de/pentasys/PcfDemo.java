package de.pentasys;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PcfDemo {

    public static void main(String[] args) {
        SpringApplication.run(PcfDemo.class, args);
    }

}
