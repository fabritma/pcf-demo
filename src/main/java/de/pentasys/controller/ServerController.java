package de.pentasys.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

@RestController
@RequestMapping("/api")
public class ServerController {

    @GetMapping(value = "/burn-cpu", produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> burnCpu() {
        Random random = new Random();

        // generate cpu traffic (ugly source due to compiler optimization)
        long i = 0;
        int  e = 0;
        while (e < 1000000) {
            e++;
            random.setSeed(i);
            i = random.nextLong();
        }

        return new ResponseEntity<>("This is some stuff from the backend", HttpStatus.OK);
    }
}
