package de.pentasys.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;
import java.time.Instant;

@RestController
@RequestMapping("/api")
public class ClientController {

    @GetMapping(produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> loadTestData() {
        return new ResponseEntity<>("localhost:8080/api/denial-of-service?loop-count=100", HttpStatus.OK);
    }

    @GetMapping(value = "/denial-of-service", produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> dos(
            @RequestParam(value = "loop-count", required = false, defaultValue = "1") Long loopCount) {

        // metric [s]
        Long responseTimeTotal = 0L;
        Long minResponseTime   = 0L;
        Long maxResponseTime   = 0L;

        RestTemplate restTemplate = new RestTemplate();
        for (long i = 0; i < loopCount; i++) {
            // start metric
            Instant start = Instant.now();

            restTemplate.getForEntity("https://spring-boot-pentasys.cfapps.io/api/burn-cpu",
                                      String.class);

            // update metric
            Long duration = Duration.between(start, Instant.now()).toMillis();
            responseTimeTotal += duration;
            minResponseTime = minResponseTime > duration || minResponseTime == 0L ? duration : minResponseTime;
            maxResponseTime = maxResponseTime < duration ? duration : maxResponseTime;
        }

        StringBuilder metricAsText = new StringBuilder();
        metricAsText.append("METRICS: \n\n")
                .append("REQUESTS_PER_SECOND: ")
                .append(loopCount / (responseTimeTotal / 1000.0))
                .append("\n")
                .append("RESPONSE_TIME_TOTAL: ")
                .append(responseTimeTotal)
                .append("\n")
                .append("AVG_RESPONSE_TIME: ")
                .append(responseTimeTotal / loopCount)
                .append("\n")
                .append("MIN_RESPONSE_TIME: ")
                .append(minResponseTime)
                .append("\n")
                .append("MAX_RESPONSE_TIME: ")
                .append(maxResponseTime)
                .append("\n");

        return new ResponseEntity<>(metricAsText.toString(), HttpStatus.OK);
    }

}
